#!/usr/bin/python3
"""
 Echo client / UDP
 Parameters: set below: server IP address, port, timeouts
 Messages are random generated, binary; length random from 8 to 300 bytes

 3-dec-2019 pavel_a@fastmail.fm 
"""
import socket
import sys, time
import random


PORT    = 7 #standard echo server
SERVER  = "10.1.1.21"
#SERVER,PORT='localhost',10000
REPLY_TIMEOUT = 0.4 # device reply timeout, sec
N_ITER = 1000         # Number of repeats
print_OK = True  # If print OK results. False=print only errors

def make_random_msg():
    # Create random echo test message
    nbytes = random.randrange(8, 300)
    x = bytearray( [ random.randrange(256) for i in range(nbytes) ] )
    return x

print("UDP echo client -> IP: %s port: %u, repeat: %u timeout=%r s" % (SERVER,PORT, N_ITER, REPLY_TIMEOUT))

# Create UDP socket
server_address = (SERVER, PORT)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

max_delay, min_delay = 0.0, 0.0 # ns
first_delay = 0.0
num_errs = 0

sock.settimeout(REPLY_TIMEOUT)
for iter in range(1, N_ITER+1):
    #time.sleep(1.0)
    try:
        # Send data & wait reply
        #message = b'This is the message.  It will be repeated.'
        message = make_random_msg()
        amount_expected = len(message)
        amount_received = 0

        #print('\n\nSending len=%u "%r"' % (amount_expected, message))
        sock.sendto(message, server_address)

        # Wait for the response
        time_sent = time.monotonic_ns()

        reply, host_from = sock.recvfrom(2048) # arg=buffer size, power of 2:
        amount_received = len(reply)
        #print('\nReceived "%r"' % reply)
        #print("From:", host_from)

        delay = (time.monotonic_ns() - time_sent) # reply latency, ns
        if iter > 1:
            max_delay = max(max_delay, delay)
            min_delay = min(min_delay, delay)
        else:
            first_delay = delay

        if reply != message:
            print("Echo #%u error: data mismatch len=%u" % (iter,amount_received) )
            num_errs += 1
        elif print_OK:
            print("Reply #%u OK, delay=%.1f ms" % (iter, (delay*1.0e-6)) )

    except OSError as e:
        print("\nRX #%u failed: %r\n\n" % (iter, e))
        num_errs += 1
    except Exception as e:
        print("Unhandled exception:", e)

print('Closing socket')
sock.close()

# print stats
print("Errors: %u of %u" % (num_errs,N_ITER))
print("1st.delay= %.1f Max.delay= %.1f Min.delay= %.1f ms" % (first_delay*1.0e-6, max_delay*1.0e-6, min_delay*1.0e-6))
