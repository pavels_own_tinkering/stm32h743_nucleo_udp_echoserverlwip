LwIP_UDP_Echo_Server for NUCLEO H7 ported from EVAL2
Cube H7 lib: 
/usr/src/STM32Cube/Repository/STM32Cube_FW_H7_V1.9

LINKED FOLDERS

STM32Cube_FW_H7 = /usr/src/STM32Cube/Repository/STM32Cube_FW_H7_V1.9  <<< + .0

BUILD VARS

STM32Cube_FW_H7 = /usr/src/STM32Cube/Repository/STM32Cube_FW_H7_V1.9 <<< + .0
LWIP_SRC = ${STM32Cube_FW_H7}/Middlewares/Third_Party/LwIP


BSP: 3 LEDS, debug UART

Link script @ RAM

Static IP address: 10.1.1.21, change in main.h. No DHCP.

To test UDP server, run udp_echoclient.py. 
To change address & port : edit in the file.
