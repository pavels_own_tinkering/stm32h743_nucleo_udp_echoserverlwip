/**
  ******************************************************************************
  * @file    LwIP/LwIP_UDP_Echo_Server/Inc/main.h
  * @author  MCD Application Team
  * @brief   Header for main.c module
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#ifdef USE_STM32H743I_EVAL
#include "stm32h743i_eval.h"
#include "stm32h743i_eval_lcd.h"
#include "stm32h743i_eval_sdram.h"
#include "stm32_lcd.h"
#elif defined(USE_NUCLEO_H743)
#include "stm32h7xx_nucleo.h"
#include "stm32h7xx_hal.h"
#endif
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#ifdef USE_STM32H743I_EVAL
#define USE_LCD
#endif

/* UDP local connection port */
#define UDP_SERVER_PORT    7
/* UDP remote connection port */
#define UDP_CLIENT_PORT    7

/*Static IP ADDRESS: IP_ADDR0.IP_ADDR1.IP_ADDR2.IP_ADDR3 */
#if 0
#define IP_ADDR0   (uint8_t) 192
#define IP_ADDR1   (uint8_t) 168
#define IP_ADDR2   (uint8_t) 0
#define IP_ADDR3   (uint8_t) 10

/*Gateway Address*/
#define GW_ADDR0   (uint8_t) 192
#define GW_ADDR1   (uint8_t) 168
#define GW_ADDR2   (uint8_t) 0
#define GW_ADDR3   (uint8_t) 1
#else
#define IP_ADDR0   (uint8_t) 10
#define IP_ADDR1   (uint8_t) 1
#define IP_ADDR2   (uint8_t) 1
#define IP_ADDR3   (uint8_t) 21

/*Gateway Address*/
#define GW_ADDR0   (uint8_t) 10
#define GW_ADDR1   (uint8_t) 1
#define GW_ADDR2   (uint8_t) 1
#define GW_ADDR3   (uint8_t) 1
#endif

/*NETMASK*/
#define NETMASK_ADDR0   (uint8_t) 255
#define NETMASK_ADDR1   (uint8_t) 255
#define NETMASK_ADDR2   (uint8_t) 255
#define NETMASK_ADDR3   (uint8_t) 0

/* Exported macro ------------------------------------------------------------*/
//+pa01
#define dbgprintf printf
#define dbgputs puts
#define ttyprintf printf
#define ttyputs puts
/* Exported functions ------------------------------------------------------- */
 extern void Error_Handler(void);

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

